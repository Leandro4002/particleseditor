# ParticlesEditor
Particles editor for the [MonoGameUtilities](https://gitlab.com/Leandro4002/monogameutilities) repository.
Made with [MonoGame 3.8](https://www.monogame.net/) and [ImGui .NET](https://github.com/mellinoe/ImGui.NET)

<img src="https://i.imgur.com/8xNzfHk.gif"/><br/>
<img src="https://i.imgur.com/0TWlpXL.gif" width="137" height="175" />
<img src="https://i.imgur.com/5Uifm75.gif" width="137" height="175" />
<img src="https://i.imgur.com/cGPRf6Q.gif" width="137" height="175" />
<img src="https://i.imgur.com/oxzFXY6.gif" width="137" height="175" />
<img src="https://i.imgur.com/IX0W0uy.gif" width="137" height="175" />
<img src="https://i.imgur.com/eaFigfp.gif" width="137" height="175" />
<img src="https://i.imgur.com/Xd95ESy.gif" width="137" height="175" />