﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGameUtilities;
using ImGuiNET;

namespace ParticlesEditor
{
    public partial class ParticlesEditor : Game {
        ImGuiIOPtr ioImGui;
        ImGuiStylePtr styleImGui;
        ImGuiWindowFlags fpsFlags, mainFlags;
        IntPtr imGuiFadePointsTexture, imGuiPreviewTexture;
        float previewLifetime, previewOpacity, previewsPerSeconds = 0.3f;
        int dragWidth = 150, comboWidth = 200, inlineTooltipWidth = 300, fadePointsGraphSize = 250;
        System.Numerics.Vector4 fadePointsGraphBorderColor = new System.Numerics.Vector4(1, 1, 1, 0.5f),
            warningColor = new System.Numerics.Vector4(1, 1, 0, 1),
            particlesColor;
        System.Numerics.Vector2 localMousePos, oldLocalMousePos;
        System.Numerics.Vector2 exportTextSize, copyClipboardButtonSize, menuSize;
        string selectedTexture = "smoke1_f12w32h32c6r2", exportText = "";
        Type[] particlesDemoTypes;
        Type selectedParticlesDemo;
        float timeSpeed = 1;
        int particlesFps, totalNumberOfParticles;
        Delay emitDelay = new Delay(0.1f);

        RenderTarget2D fadePointsRenderTarget;
        Vector2[] bufferLinearFadePoints, bufferFunctionalBezierFadePoints;
        Vector2 graphLabelPosition;
        Texture2D graphTexture, gridTexture, pointTexture, previewPointTexture, graphLabelTexture;
        bool fadePointsGraphNeedsRefresh;
        int selectedPoint;

        void UIInitialize() {
            ioImGui = ImGui.GetIO();

            fpsFlags = ImGuiWindowFlags.NoMove | ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoTitleBar | ImGuiWindowFlags.NoBackground;
            mainFlags = ImGuiWindowFlags.NoMove | ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoTitleBar | ImGuiWindowFlags.NoBringToFrontOnFocus;

            styleImGui = ImGui.GetStyle();
            styleImGui.WindowRounding = 0;
            styleImGui.Colors[(int)ImGuiCol.TitleBg] = styleImGui.Colors[(int)ImGuiCol.TitleBgActive];
            styleImGui.FramePadding.X = 20;

            copyClipboardButtonSize = new System.Numerics.Vector2(180, 40);
            menuSize = new System.Numerics.Vector2(350, graphics.PreferredBackBufferHeight);
            
            bufferFunctionalBezierFadePoints = new Vector2[] { new Vector2(0, 1), new Vector2(0.25f, 0), new Vector2(0.75f, 1), new Vector2(1, 0) };
            bufferLinearFadePoints = new Vector2[] { new Vector2(0, 1), new Vector2(1, 0) };
            fadePointsRenderTarget = new RenderTarget2D(GraphicsDevice, fadePointsGraphSize, fadePointsGraphSize);
            gridTexture = GraphicsDevice.CreateGridTexture(5, 5, fadePointsGraphSize / 5, new Color(1f, 1f, 1f, 0.1f));
            pointTexture = GraphicsDevice.CreateCircleTexture(10, Color.Red);
            previewPointTexture = GraphicsDevice.CreateCircleTexture(6, Color.Blue);
            graphLabelTexture = Content.Load<Texture2D>("graphLabel");
            graphLabelPosition = new Vector2(5, 198);

            fadePointsGraphNeedsRefresh = true;
        }

        void UIUpdate(float dt) {
            if (particleSystem.fadeOutMethod != ParticleSystem.FadeOutMethod.None) {
                if (previewsPerSeconds < 0) return;
                previewLifetime += dt * previewsPerSeconds;

                if (previewLifetime >= 1) previewLifetime = 0;

                if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.Linear) {
                    previewOpacity = ParticleSystem.CalculateOpacityLinear(particleSystem.fadePoints, previewLifetime);
                } else if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.FunctionalBezier) {
                    previewOpacity = ParticleSystem.CalculateOpacityFunctionalBezier(particleSystem.fadePoints, previewLifetime);
                }
            }
        }

        void UIDraw() {
            ImGui.Begin("Particle editor", mainFlags);
            ImGui.SetWindowPos(System.Numerics.Vector2.Zero);
            ImGui.SetWindowSize(menuSize);
            if (ImGui.BeginTabBar("MainTabBar")) {
                ImGui.PushItemWidth(dragWidth);
                if (ImGui.BeginTabItem("Params")) {
                    ImGui.TextWrapped("Press left click on a parameter and move your mouse to change it's value. Press shift while dragging to change value faster.");
                    ImGui.DragFloat("minScale", ref particleSystem.minScale, 0.01f, 0); InlineTooltip("Minimum scale of each particle.");
                    ImGui.DragFloat("maxScale", ref particleSystem.maxScale, 0.01f, 0); InlineTooltip("Maximum scale of each particle.");
                    ImGui.DragFloat("minLifeDuration", ref particleSystem.minLifeDuration, 0.005f, 0); InlineTooltip("Minimum life duration of a particle. Only works when particleDeath is set to `onEndOfLifeDuration`.");
                    ImGui.DragFloat("maxLifeDuration", ref particleSystem.maxLifeDuration, 0.005f, 0); InlineTooltip("Maximum life duration of a particle. Only works when particleDeath is set to `onEndOfLifeDuration`.");
                    ImGui.DragFloat("minAcceleration", ref particleSystem.minAcceleration, 1f); InlineTooltip("Minimum acceleration of particle in the same direction as `emitDirection`. Can be negative.");
                    ImGui.DragFloat("maxAcceleration", ref particleSystem.maxAcceleration, 1f); InlineTooltip("Maximum acceleration of particle in the same direction as `emitDirection`. Can be negative.");
                    ImGui.DragFloat("minInitialSpeed", ref particleSystem.minInitialSpeed, 0.5f); InlineTooltip("Minimum initial speed of particle in the same direction as `emitDirection`. Can be negative.");
                    ImGui.DragFloat("maxInitialSpeed", ref particleSystem.maxInitialSpeed, 0.5f); InlineTooltip("Maximum initial speed of particle in the same direction as `emitDirection`. Can be negative.");
                    ImGui.DragFloat("minRotationSpeed", ref particleSystem.minRotationSpeed, 0.1f); InlineTooltip("Minimum rotation speed of particle. Can be clockwise or anticlockwise.");
                    ImGui.DragFloat("maxRotationSpeed", ref particleSystem.maxRotationSpeed, 0.1f); InlineTooltip("Maximum rotation speed of particle. Can be clockwise or anticlockwise.");
                    ImGui.DragFloat("minInitialRotation", ref particleSystem.minInitialRotation, 0.01f, -MathHelper.TwoPi, MathHelper.TwoPi); InlineTooltip("Minimum initial rotation of particle.");
                    ImGui.DragFloat("maxInitialRotation", ref particleSystem.maxInitialRotation, 0.01f, -MathHelper.TwoPi, MathHelper.TwoPi); InlineTooltip("Maximum initial rotation of particle.");
                    ImGui.DragInt("minNumParticles", ref particleSystem.minNumParticles, 0.1f, 0, 10000); InlineTooltip("Minimum number of particles for each emit of particles.");
                    ImGui.DragInt("maxNumParticles", ref particleSystem.maxNumParticles, 0.1f, 0, 10000); InlineTooltip("Maximum number of particles for each emit of particles.");
                    if (ImGui.DragInt("totalNumOfParticles", ref totalNumberOfParticles, 0.1f, 1, 100000)) { particleSystem.TotalNumberOfParticles = totalNumberOfParticles; } InlineTooltip("Number of particles that can be on the screen at once.");
                    ImGui.DragFloat("emitAngle", ref particleSystem.emitAngle, 0.01f, 0, MathHelper.TwoPi); InlineTooltip("Angle where the particles go when emitting, in the direction of `emitDirection`.");
                    ImGui.DragFloat("emitDirection", ref particleSystem.emitDirection, 0.01f, 0, MathHelper.TwoPi); InlineTooltip("Direction at which the particles go when particles they are are emitted.");
                    if (particleSystem != null && particleSystem.CustomParticleDirection != null) {
                        ImGui.PushTextWrapPos(menuSize.X);
                        ImGui.TextColored(warningColor, "a CustomParticleDirection delegate is set for the current particleSystem, emitWideness and emitDirection may not be used.");
                        ImGui.PopTextWrapPos();
                    }
                    if (ImGui.ColorPicker4("ParticlesColor", ref particlesColor, ImGuiColorEditFlags.AlphaBar | ImGuiColorEditFlags.AlphaPreviewHalf)) {
                        particleSystem.ParticlesColor = new Color(particlesColor.X, particlesColor.Y, particlesColor.Z, particlesColor.W);
                    } InlineTooltip("Color filter applied to each particles. Each particle can have a different color, but in this editor we put the same color for each particle.");
                    ImGui.Separator();
                    ImGui.TextWrapped("The parameters below changes the way this editor handles the particle system. Those are not fields from particleSystem.");
                    ImGui.DragFloat("emitDelay.timer", ref emitDelay.timer, 0.001f, 0.0f, 3f); InlineTooltip("Number of seconds between each emit of particles.");
                    if (ImGui.DragInt("particleFps", ref particlesFps, 0.1f, 0)) {
                        particleSystem.ParticlesFps = particlesFps;
                        particleSystem.ConstructData(particleSystem.TotalNumberOfParticles);
                    }; InlineTooltip("Framerate of the particle's animated sprite.");
                    ImGui.DragFloat("timeSpeed", ref timeSpeed, 0.001f, 0); InlineTooltip("Speed of the simulation.");
                    ImGui.Checkbox("showDebug", ref particleSystem.showDebug); InlineTooltip("Display particleSystem origin, particles origin and particles direction.");
                    ImGui.EndTabItem();
                }
                if (ImGui.BeginTabItem("Other")) {
                    ImGui.PushItemWidth(comboWidth);
                    if (ImGui.BeginCombo("particleSystems", selectedParticlesDemo.Name)) {
                        foreach (Type demoType in particlesDemoTypes) {
                            if (ImGui.Selectable(demoType.Name, demoType == selectedParticlesDemo)) {
                                selectedParticlesDemo = demoType;
                                InstanciateParticleSystem(selectedParticlesDemo);
                            }
                        }
                        ImGui.EndCombo();
                    }
                    InlineTooltip("[ C A U T I O N ] This will change all values of the current particleSystem! Samples taken from MonoGameUtilities's demo.", isImportant: true);

                    if (ImGui.BeginCombo("textures", CleanTextureName(selectedTexture))) {
                        foreach (KeyValuePair<string, Texture2D> particleTexture in particleTextures) {
                            bool isSelected = particleTexture.Key == selectedTexture;
                            if (ImGui.Selectable(CleanTextureName(particleTexture.Key), isSelected)) {
                                selectedTexture = particleTexture.Key;
                                particleSystem.Texture = particleTexture.Value;
                                particleSystem.ConstructData(particleSystem.TotalNumberOfParticles);
                            }
                            if (isSelected) ImGui.SetItemDefaultFocus();
                        }
                        ImGui.EndCombo();
                    }
                    if (ImGui.IsItemClicked()) {
                        LoadParticlesTexture();
                    }
                    InlineTooltip("Particle textures located in the `/particles_texture` path.");
                    ImGui.NewLine();

                    ImGui.PushItemWidth(dragWidth);

                    ImGui.Text("particleSystem.DeathTrigger"); InlineTooltip("Define how particles disapear.");
                    if (ImGui.RadioButton("OnEndOfAnimation", particleSystem.deathTrigger == ParticleSystem.DeathTrigger.OnEndOfAnimation)) {
                        particleSystem.deathTrigger = ParticleSystem.DeathTrigger.OnEndOfAnimation;
                        particleSystem.ConstructData(particleSystem.TotalNumberOfParticles);
                    }
                    if (ImGui.RadioButton("onEndOfLifeDuration", particleSystem.deathTrigger == ParticleSystem.DeathTrigger.onEndOfLifeDuration)) {
                        particleSystem.deathTrigger = ParticleSystem.DeathTrigger.onEndOfLifeDuration;
                        particleSystem.ConstructData(particleSystem.TotalNumberOfParticles);
                    }
                    ImGui.NewLine();
                    ImGui.Text("particleSystem.FadeOutMethod"); InlineTooltip("Determines how particles progressively loose opacity until they disapear. Needs `particleSystem.fadePoints` to determine fade out");
                    if (ImGui.RadioButton("None", particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.None)) { particleSystem.fadeOutMethod = ParticleSystem.FadeOutMethod.None; }
                    if (ImGui.RadioButton("Linear", particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.Linear)) {
                        particleSystem.fadeOutMethod = ParticleSystem.FadeOutMethod.Linear;
                        particleSystem.fadePoints = bufferLinearFadePoints;
                        fadePointsGraphNeedsRefresh = true;
                    }
                    if (ImGui.RadioButton("FunctionalBezier", particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.FunctionalBezier)) {
                        particleSystem.fadeOutMethod = ParticleSystem.FadeOutMethod.FunctionalBezier;
                        particleSystem.fadePoints = bufferFunctionalBezierFadePoints;
                        fadePointsGraphNeedsRefresh = true;
                    }

                    if (particleSystem.fadeOutMethod != ParticleSystem.FadeOutMethod.None) {
                        ImGui.DragFloat("previewsPerSeconds", ref previewsPerSeconds, 0.0005f, 0); InlineTooltip("Preview speed of the particle fade out (the little fireball below). This value has no influence in the particleSystem.");

                        ImGui.Columns(2);
                        ImGui.SetColumnWidth(0, fadePointsGraphSize + styleImGui.WindowPadding.X + 10);
                        ImGui.Image(imGuiFadePointsTexture, new System.Numerics.Vector2(fadePointsGraphSize, fadePointsGraphSize), System.Numerics.Vector2.Zero, System.Numerics.Vector2.One, System.Numerics.Vector4.One, fadePointsGraphBorderColor);
                        if (ImGui.IsItemHovered()) {
                            localMousePos = ImGui.GetMousePos() - ImGui.GetItemRectMin();
                            
                            if (ImGui.IsMouseClicked(ImGuiMouseButton.Left)) {
                                for (int i = 0; i < particleSystem.fadePoints.Length; i++) {
                                    if (Tools.Pos2Circle(localMousePos.X, localMousePos.Y, particleSystem.fadePoints[i].X * fadePointsGraphSize, (1 - particleSystem.fadePoints[i].Y) * fadePointsGraphSize, pointTexture.Width / 2)) {
                                        selectedPoint = i;
                                        break;
                                    }
                                }
                            }

                            if (ImGui.IsMouseDown(ImGuiMouseButton.Left) && selectedPoint != -1) {
                                System.Numerics.Vector2 deltaMouse = (localMousePos - oldLocalMousePos) / fadePointsGraphSize;
                                deltaMouse.Y = -deltaMouse.Y;
                                particleSystem.fadePoints[selectedPoint] += new Vector2(deltaMouse.X, deltaMouse.Y);
                                if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.Linear) {
                                    particleSystem.fadePoints[selectedPoint].X = MathHelper.Clamp(particleSystem.fadePoints[selectedPoint].X, 0, 1);
                                    particleSystem.fadePoints[selectedPoint].Y = MathHelper.Clamp(particleSystem.fadePoints[selectedPoint].Y, 0, 1);
                                } else if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.FunctionalBezier && (selectedPoint == 1 || selectedPoint == 2)) {
                                    if (selectedPoint == 1) particleSystem.fadePoints[selectedPoint].X = 0.25f;
                                    if (selectedPoint == 2) particleSystem.fadePoints[selectedPoint].X = 0.75f;
                                }

                                if (selectedPoint == 0) particleSystem.fadePoints[selectedPoint].X = 0;
                                if (selectedPoint == particleSystem.fadePoints.Length - 1) particleSystem.fadePoints[selectedPoint].X = 1;

                                if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.Linear) {
                                    if (selectedPoint > 0 && particleSystem.fadePoints[selectedPoint - 1].X > particleSystem.fadePoints[selectedPoint].X) {
                                        particleSystem.fadePoints[selectedPoint].X = particleSystem.fadePoints[selectedPoint - 1].X;
                                    } else if (selectedPoint < particleSystem.fadePoints.Length - 1 && particleSystem.fadePoints[selectedPoint].X > particleSystem.fadePoints[selectedPoint + 1].X) {
                                        particleSystem.fadePoints[selectedPoint].X = particleSystem.fadePoints[selectedPoint + 1].X;
                                    }
                                    bufferLinearFadePoints = particleSystem.fadePoints;
                                } else if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.FunctionalBezier) {
                                    bufferFunctionalBezierFadePoints = particleSystem.fadePoints;
                                }

                                fadePointsGraphNeedsRefresh = true;
                            } else {
                                selectedPoint = -1;
                            }

                            oldLocalMousePos = localMousePos;
                        }
                        ImGui.NextColumn();
                        ImGui.Text(string.Format("lifetime\n{0:N}\n\nopacity\n{1:N}", previewLifetime, previewOpacity));
                        ImGui.NewLine();
                        ImGui.Image(imGuiPreviewTexture, new System.Numerics.Vector2(32), System.Numerics.Vector2.Zero, System.Numerics.Vector2.One, new System.Numerics.Vector4(1, 1, 1, previewOpacity));
                        ImGui.Columns(1);
                        if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.Linear) {
                            if (ImGui.Button("Add a fade point")) {
                                bufferLinearFadePoints = new Vector2[particleSystem.fadePoints.Length + 1];
                                for (int i = 0; i < particleSystem.fadePoints.Length; i++) {
                                    bufferLinearFadePoints[i] = particleSystem.fadePoints[i];
                                    if (i > 0) { bufferLinearFadePoints[i].X -= (bufferLinearFadePoints[i].X - bufferLinearFadePoints[i - 1].X) / 2; }
                                }
                                bufferLinearFadePoints[particleSystem.fadePoints.Length] = new Vector2(1, bufferLinearFadePoints[bufferLinearFadePoints.Length - 2].Y);
                                particleSystem.fadePoints = bufferLinearFadePoints;
                                fadePointsGraphNeedsRefresh = true;
                            }
                            if (ImGui.Button("Remove last fade point") && particleSystem.fadePoints.Length > 2) {
                                bufferLinearFadePoints = new Vector2[particleSystem.fadePoints.Length - 1];
                                for (int i = 0; i < particleSystem.fadePoints.Length - 1; i++) {
                                    bufferLinearFadePoints[i] = particleSystem.fadePoints[i];
                                    if (i == bufferLinearFadePoints.Length - 1) { bufferLinearFadePoints[i].X = 1; }
                                }
                                particleSystem.fadePoints = bufferLinearFadePoints;
                                fadePointsGraphNeedsRefresh = true;
                            }
                        } else if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.FunctionalBezier) {
                            if (ImGui.DragFloat("Ease in", ref bufferFunctionalBezierFadePoints[1].Y, 0.001f)) {
                                particleSystem.fadePoints = bufferFunctionalBezierFadePoints;
                                fadePointsGraphNeedsRefresh = true;
                            }
                            if (ImGui.DragFloat("Ease out", ref bufferFunctionalBezierFadePoints[2].Y, 0.001f)) {
                                particleSystem.fadePoints = bufferFunctionalBezierFadePoints;
                                fadePointsGraphNeedsRefresh = true;
                            }
                        }
                    }
                    ImGui.EndTabItem();
                }
                if (ImGui.BeginTabItem("Export")) {
                    ImGui.TextWrapped("You need to copy this code inside the `InitializeConstant` on your `ParticleSystem` sublclass.");
                    ImGui.Separator();
                    ImGui.NewLine();
                    ImGui.BeginChild("exportText", exportTextSize, false, ImGuiWindowFlags.HorizontalScrollbar);
                    ImGui.Text(exportText);
                    ImGui.EndChild();
                    if (ImGui.Button("Copy to clipboard", copyClipboardButtonSize)) ImGui.SetClipboardText(exportText);
                    ImGui.EndTabItem();
                }
                if (ImGui.IsItemClicked()) RefreshExportText();
                ImGui.EndTabBar();
            }
            ImGui.End();
            
            ImGui.Begin("FPS", fpsFlags);
            string fpsString = ioImGui.Framerate.ToString("0") + " FPS  ";
            ImGui.SetWindowPos(new System.Numerics.Vector2(menuSize.X - ImGui.CalcTextSize(fpsString).X - styleImGui.ScrollbarSize, 0));
            ImGui.Text(fpsString);
            ImGui.End();
        }

        void InlineTooltip(string text, bool isImportant = false) {
            ImGui.SameLine();
            if (isImportant) ImGui.TextColored(warningColor, "(?)");
            else ImGui.TextDisabled("(?)");
            if (ImGui.IsItemHovered()) {
                ImGui.BeginTooltip();
                ImGui.PushTextWrapPos(inlineTooltipWidth);
                if (isImportant) ImGui.TextColored(warningColor, text);
                else ImGui.TextUnformatted(text);
                ImGui.PopTextWrapPos();
                ImGui.EndTooltip();
            }
        }

        void RefreshFadePointsGraph() {
            GraphicsDevice.SetRenderTarget(fadePointsRenderTarget);
            GraphicsDevice.Clear(Color.Transparent);
            spriteBatch.Begin();

            spriteBatch.Draw(gridTexture, Vector2.Zero, Color.White);
            spriteBatch.Draw(graphLabelTexture, graphLabelPosition, Color.White);
            if (particleSystem.fadePoints != null) {
                if (fadePointsGraphNeedsRefresh) {
                    if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.Linear) {
                        graphTexture = GraphicsDevice.CreateLineTexture(new List<Vector2>(particleSystem.fadePoints), Color.Red, fadePointsGraphSize, invertYAxis: true);
                    } else if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.FunctionalBezier) {
                        graphTexture = FunctionalBezier.GenerateCurve(GraphicsDevice, fadePointsGraphSize, 1000, Color.Red, particleSystem.fadePoints, invertYAxis: true);
                    }
                    fadePointsGraphNeedsRefresh = false;
                }
                spriteBatch.Draw(graphTexture, Vector2.Zero, Color.White);
                spriteBatch.Draw(previewPointTexture, new Vector2(previewLifetime * fadePointsGraphSize, (1 - previewOpacity) * fadePointsGraphSize) - previewPointTexture.Bounds.Size.ToVector2() / 2, Color.White);
                for (int i = 0; i < particleSystem.fadePoints.Length; i++) {
                    Color pointColor = (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.FunctionalBezier && (i == 1 || i == 2)) ? Color.Gray : Color.White;
                    Vector2 pointDrawPos = new Vector2(particleSystem.fadePoints[i].X, 1 - particleSystem.fadePoints[i].Y) * fadePointsGraphSize;
                    spriteBatch.Draw(pointTexture, pointDrawPos + new Vector2(-pointTexture.Width / 2), pointColor);
                    spriteBatch.DrawString(spriteFont, i.ToString(), pointDrawPos + new Vector2(-5, -7), pointColor);
                }
            }

            spriteBatch.End();
            GraphicsDevice.SetRenderTarget(null);

            imGuiFadePointsTexture = _imGuiRenderer.BindTexture(fadePointsRenderTarget);
        }

        string CleanTextureName(string textureName) => textureName.Substring(0, textureName.IndexOf("_"));

        string FormatNumber(object number, int roundDigit = 4) {
            if (number is int) {
                return number.ToString();
            } else if (number is float) {
                float floatNumber = (float)number;
                if (roundDigit > 0) floatNumber = (float)Math.Round(floatNumber, roundDigit);
                string formattedNumber = floatNumber.ToString(System.Globalization.CultureInfo.InvariantCulture);
                if (formattedNumber.Contains('.')) formattedNumber += "f";
                return formattedNumber;
            }
            throw new Exception("Wrong Type, Format Number only accept float and int");
        }
        void RefreshExportText() {
            exportText = "";
            Tuple<string, object, object>[] parameters = new Tuple<string, object, object>[] {
                new Tuple<string, object, object>("Scale", particleSystem.minScale, particleSystem.maxScale),
                new Tuple<string, object, object>("LifeDuration", particleSystem.minLifeDuration, particleSystem.maxLifeDuration),
                new Tuple<string, object, object>("Acceleration", particleSystem.minAcceleration, particleSystem.maxAcceleration),
                new Tuple<string, object, object>("InitialSpeed", particleSystem.minInitialSpeed, particleSystem.maxInitialSpeed),
                new Tuple<string, object, object>("RotationSpeed", particleSystem.minRotationSpeed, particleSystem.maxRotationSpeed),
                new Tuple<string, object, object>("InitialRotation", particleSystem.minInitialRotation, particleSystem.maxInitialRotation),
                new Tuple<string, object, object>("NumParticles", particleSystem.minNumParticles, particleSystem.maxNumParticles),
            };
            foreach (Tuple<string, object, object> parameter in parameters) {
                exportText += string.Format("min{0} = {1}; max{0} = {2}", parameter.Item1, FormatNumber(parameter.Item2), FormatNumber(parameter.Item3));
                exportText += ";" + Environment.NewLine;
            }

            exportText += "TotalNumberOfParticles = " + particleSystem.TotalNumberOfParticles + ";" + Environment.NewLine;
            exportText += "emitAngle = " + FormatNumber(particleSystem.emitAngle) + ";" + Environment.NewLine;
            exportText += "emitDirection = " + FormatNumber(particleSystem.emitDirection) + ";" + Environment.NewLine;
            exportText += string.Format("ParticlesColor = new Color({0},{1},{2},{3});" + Environment.NewLine,
                particleSystem.ParticlesColor.R, particleSystem.ParticlesColor.G, particleSystem.ParticlesColor.B, particleSystem.ParticlesColor.A);

            string enumName = Enum.GetName(typeof(ParticleSystem.FadeOutMethod), particleSystem.fadeOutMethod);
            exportText += "fadeOutMethod = ParticleSystem.FadeOutMethod." + enumName + ";";

            if (particleSystem.fadeOutMethod != ParticleSystem.FadeOutMethod.None) {
                string[] stringFadePoints = new string[particleSystem.fadePoints.Length];
                int i = 0;
                foreach (Vector2 vector2 in particleSystem.fadePoints) {
                    stringFadePoints[i] = "new Vector2(" + FormatNumber(vector2.X, 2) + ", " + FormatNumber(vector2.Y, 2) + ")";
                    i++;
                }
                exportText += Environment.NewLine + "fadePoints = new Vector2[] {" + string.Join(", ", stringFadePoints) + "};";
            }

            exportText += Environment.NewLine + "deathTrigger = ParticleSystem.DeathTrigger." + particleSystem.deathTrigger.ToString() + ";";

            exportTextSize = new System.Numerics.Vector2(320, ImGui.CalcTextSize(exportText).Y + 20);
        }
    }
}