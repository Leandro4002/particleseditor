using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.IO;
using MonoGameUtilities;
using System;
using System.Reflection;
using System.Linq;
using System.Diagnostics;

namespace ParticlesEditor {
    public partial class ParticlesEditor : Game {
        ParticleSystem particleSystem;
        SpriteFont spriteFont;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        MouseState mouseState, oldMouseState;
        Dictionary<string, Texture2D> particleTextures;
        ImGuiRenderer _imGuiRenderer;
        struct particleSystemSampleCustomVal {
            public particleSystemSampleCustomVal(Type type, string textureName, int particleFps, float timer) {
                this.type = type;
                this.textureName = textureName;
                this.particleFps = particleFps;
                this.timer = timer;
            }
            public Type type;
            public string textureName;
            public int particleFps;
            public float timer;
        }
        particleSystemSampleCustomVal[] customValForParticleSystemSamples;

        public ParticlesEditor() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize() {
            graphics.PreferredBackBufferWidth = 900;
            graphics.PreferredBackBufferHeight = 700;
            graphics.ApplyChanges();

            particlesDemoTypes = Assembly.Load("MonoGameUtilities").GetTypes().Where(t => string.Equals(t.Namespace, "DemoParticles", StringComparison.Ordinal)).ToArray();

            _imGuiRenderer = new ImGuiRenderer(this);
            UIInitialize();

            particleTextures = new Dictionary<string, Texture2D>();

            base.Initialize();
        }

        void LoadParticlesTexture() {
            string[] files = Directory.GetFiles("particles_texture", "*.png", SearchOption.AllDirectories);
            particleTextures = new Dictionary<string, Texture2D>();
            List<string> texturesFound = new List<string>();
            foreach (string file in files) {
                FileStream fileStream = new FileStream(file, FileMode.Open);
                string key = file.Substring(file.LastIndexOf('\\') + 1).Replace(".png", "");
                Texture2D texture = Texture2D.FromStream(GraphicsDevice, fileStream);
                texture.Name = key;
                particleTextures.Add(key, texture);
                fileStream.Dispose();
            }
        }

        protected override void LoadContent() {
            imGuiPreviewTexture = _imGuiRenderer.BindTexture(Content.Load<Texture2D>("previewTexture"));
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteFont = Content.Load<SpriteFont>("fonts/consolas");

            LoadParticlesTexture();

            customValForParticleSystemSamples = new particleSystemSampleCustomVal[] {
                new particleSystemSampleCustomVal(typeof(DemoParticles.SmokeParticleSystem), "smoke1_f12w32h32c6r2", 15, 0.1f),
                new particleSystemSampleCustomVal(typeof(DemoParticles.HomingParticleSystem), "flame5_f60w100h100c8r8", 30, 0.01f),
                new particleSystemSampleCustomVal(typeof(DemoParticles.BounceParticleSystem), "hit_f31w100h100c6r6", 15, 0.1f),
                new particleSystemSampleCustomVal(typeof(DemoParticles.ConstantEmitParticleSystem), "freeze_f86w100h100c10r10", 30, 0.1f),
                new particleSystemSampleCustomVal(typeof(DemoParticles.GravityParticleSystem), "magic6_f61w100h100c8r8", 50, 0.3f),
                new particleSystemSampleCustomVal(typeof(DemoParticles.RainParticleSystem), "rain_f1w3h20c1r1", 0, 0.01f),
                new particleSystemSampleCustomVal(typeof(DemoParticles.TrippyParticleSystem), "magic2_f61w100h100c8r8", 300, 0.01f),
            };

            selectedParticlesDemo = typeof(DemoParticles.SmokeParticleSystem);
            InstanciateParticleSystem(selectedParticlesDemo);

            CenterParticleSystemOrigin();

            if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.FunctionalBezier) bufferFunctionalBezierFadePoints = particleSystem.fadePoints;
            else if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.Linear) bufferLinearFadePoints = particleSystem.fadePoints;
        }

        protected override void Update(GameTime gameTime) {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds * timeSpeed;

            UIUpdate(dt);
            particleSystem.Update(dt);
            emitDelay.Update(dt);
            if (emitDelay.isTrigger) {
                emitDelay.Reset();
                particleSystem.EmitParticles();
            }

            RefreshFadePointsGraph();

            mouseState = Mouse.GetState();
            if (mouseState.RightButton == ButtonState.Pressed && mouseState.Position != oldMouseState.Position && mouseState.X > menuSize.X) {
                Vector2 movement = mouseState.Position.ToVector2() - oldMouseState.Position.ToVector2();
                particleSystem.origin += movement;
            }
            oldMouseState = mouseState;

            particleSystem.origin.X = MathHelper.Clamp(particleSystem.origin.X, menuSize.X, graphics.PreferredBackBufferWidth);
            particleSystem.origin.Y = MathHelper.Clamp(particleSystem.origin.Y, 0, graphics.PreferredBackBufferHeight);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, SamplerState.PointClamp);
            particleSystem.Draw();
            spriteBatch.End();

            _imGuiRenderer.BeforeLayout(gameTime);
            UIDraw();
            _imGuiRenderer.AfterLayout();

            base.Draw(gameTime);
        }

        System.Numerics.Vector4 ColorToSystemNumericsVector4(Color color) {
            Vector4 vector4Color = color.ToVector4();
            return new System.Numerics.Vector4(vector4Color.X, vector4Color.Y, vector4Color.Z, vector4Color.W);
        }

        void CenterParticleSystemOrigin() {
            particleSystem.origin = new Vector2(graphics.PreferredBackBufferWidth / 2 + menuSize.X / 2, graphics.PreferredBackBufferHeight / 2);
        }

        void InstanciateParticleSystem(Type particleSystemType) {
            bool showDebug = false;
            if (particleSystem != null) showDebug = particleSystem.showDebug;

            // Set custom texture and fps here because these values cannot be exported in a particleSystem !
            foreach (particleSystemSampleCustomVal customVal in customValForParticleSystemSamples) {
                if (particleSystemType == customVal.type) {
                    selectedTexture = customVal.textureName;
                    particlesFps = customVal.particleFps;
                    emitDelay.timer = customVal.timer;
                    break;
                }
            }

            if (!particleTextures.ContainsKey(selectedTexture)) selectedTexture = particleTextures.First().Key;

            particleSystem = (ParticleSystem)Activator.CreateInstance(particleSystemType, spriteBatch, particleTextures[selectedTexture], particlesFps);

            CenterParticleSystemOrigin();
            particleSystem.showDebug = showDebug;
            particlesColor = ColorToSystemNumericsVector4(particleSystem.ParticlesColor);
            totalNumberOfParticles = particleSystem.TotalNumberOfParticles;

            if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.Linear) bufferLinearFadePoints = particleSystem.fadePoints;
            if (particleSystem.fadeOutMethod == ParticleSystem.FadeOutMethod.FunctionalBezier) bufferFunctionalBezierFadePoints = particleSystem.fadePoints;
            fadePointsGraphNeedsRefresh = true;
        }
    }
}