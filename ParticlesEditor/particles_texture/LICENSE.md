## Explosions animations (explosionSimple1-6)
https://ansimuz.itch.io/explosion-animations-pack
Creative Commons Zero v1.0 Universal (Free for personal and commercial use, no attribution)

## Pixel effect pack (magic1-12, flame1-5, freeze, hit)
https://codemanu.itch.io/pixelart-effect-pack
Free to use for personal and commercial purpose.

## Other (explosion1)
Made by a friend. Free for personal and commercial use, no attribution.

## Smoke (smoke1, smoke2)
This is taken from minecraft default pack texture, when a villager is angry. smoke2 is juste smoke1 with less alpha. Not free for personal and commercial use, I guess

## Rain (rain)
Made by me. These 60 pixels are free for personal and commercial use, no attribution.