﻿using System;

namespace ParticlesEditor {
    public static class Program {
        public static ParticlesEditor game;

        [STAThread]
        static void Main() {
            game = new ParticlesEditor();
            game.Run();
        }
    }
}
